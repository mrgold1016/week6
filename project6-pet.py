__author__ = 'gmoran2017'

import pet

myPet = pet.Pet("Fido", "Dog", 5)
name = input("Enter your pets name: ")
myPet.set_name(name)

age = input('Enter your pets age: ')
myPet.set_age(age)

type = input('Enter your pets type: ')
myPet.set_animal_type(type)

print('you have a ' + type + ' with the name ' + name + ' and it is ' + age + ' year old')
